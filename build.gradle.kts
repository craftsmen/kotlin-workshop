fun gitVersion(): String = try {
    val process = ProcessBuilder()
            .redirectErrorStream(true)
            .command(listOf("git", "describe", "--long", "--tags", "--match", "v[0-9]*"))
            .start()
    val output = process.inputStream.bufferedReader().use { it.readText().trim() }
    if (process.waitFor() == 0) output
            .substringAfter('v')
            .substringBeforeLast('-')
            .replace('-', '.')
    else throw IllegalStateException(output)
} catch (e: Exception) {
    e.printStackTrace()
    "unspecified"
}

plugins {
    kotlin("jvm") version "1.3.50" apply false
}

group = "nl.craftsmen.workshop.kotlin"
version = gitVersion()

tasks.withType<Wrapper> {
    distributionType = Wrapper.DistributionType.ALL
    gradleVersion = "5.6"
}

subprojects {
    group = rootProject.group
    version = rootProject.version

    repositories {
        jcenter()
    }
}
