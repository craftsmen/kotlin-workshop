import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

application {
    mainClassName = "nl.craftsmen.workshop.kotlin.server.Main"
}

dependencies {
    compile(kotlin("stdlib-jdk8"))
    compile("org.glassfish:javax.json:1.1.4")
    compile("io.ktor:ktor-server-core:1.2.3")
    compile("io.ktor:ktor-server-netty:1.2.3")
    compile("org.jetbrains.exposed:exposed:0.17.1")
    runtime("org.slf4j:slf4j-jdk14:1.7.28")
    runtime("com.h2database:h2:1.4.199")
}

kotlin {
    experimental.coroutines = Coroutines.ENABLE
}
