package nl.craftsmen.workshop.kotlin.server

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.DefaultHeaders
import io.ktor.http.ContentType
import io.ktor.request.receiveStream
import io.ktor.response.respondText
import io.ktor.response.respondTextWriter
import io.ktor.routing.*
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import javax.json.Json

object Tasks : Table() {
    val name = varchar("name", length = 50).primaryKey() // Column<String>
    val description = text("description") // Column<String>
    val done = bool("done")
}

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver")
        transaction {
            Tasks.createStatement().forEach {
                exec(it)
            }
            Tasks.insert {
                it[Tasks.name] = "TornadoFX demo"
                it[Tasks.description] = "Laten zien hoe je doormiddel van TornadoFX gemakkelijk een UI kunt maken"
                it[Tasks.done] = true
            }
            Tasks.insert {
                it[Tasks.name] = "Exposed demo"
                it[Tasks.description] = "Laten zien hoe je strong-typed database queries uitvoert"
                it[Tasks.done] = true
            }
            Tasks.insert {
                it[Tasks.name] = "Ktor demo"
                it[Tasks.description] = "Laten zien hoe snel een web service opzet"
                it[Tasks.done] = true
            }
        }

        val server = embeddedServer(Netty, 8081) {
            install(DefaultHeaders)
            install(CallLogging)
            routing {
                get {
                    call.respondText("Hello, world!", ContentType.Text.Plain)
                }
                route("api") {
                    route("tasks") {
                        get {
                            val jsonArrayBuilder = Json.createArrayBuilder()
                            transaction {
                                Tasks
                                        .selectAll()
                                        .forEach { row ->
                                            jsonArrayBuilder.add(Json.createObjectBuilder()
                                                    .add("name", row[Tasks.name])
                                                    .add("description", row[Tasks.description])
                                                    .add("done", row[Tasks.done])
                                            )
                                        }
                            }
                            call.respondTextWriter(ContentType.Application.Json) {
                                Json.createWriter(this).write(jsonArrayBuilder.build())
                            }
                        }
                        accept(ContentType.Application.Json) {
                            put("{name}") {
                                val name = call.parameters["name"] ?: return@put
                                val body = Json.createReader(call.receiveStream()).readObject()
                                transaction {
                                    val rowsAffected = Tasks.update({
                                        Tasks.name.eq(name)
                                    }) {
                                        it[Tasks.name] = body.getString("name")
                                        it[Tasks.description] = body.getString("description", "")
                                        it[Tasks.done] = body.getBoolean("done")
                                    }
                                    if (rowsAffected == 0) {
                                        Tasks.insert {
                                            it[Tasks.name] = body.getString("name")
                                            it[Tasks.description] = body.getString("description", "")
                                            it[Tasks.done] = body.getBoolean("done")
                                        }
                                    }
                                }
                                call.respondTextWriter {
                                    Json.createWriter(this).write(body)
                                }
                            }
                        }
                        delete("{name}") {
                            val name = call.parameters["name"] ?: return@delete
                            transaction {
                                Tasks.deleteWhere {
                                    Tasks.name.eq(name)
                                }
                            }
                        }
                    }
                }
            }
        }
        server.start(wait = true)
    }
}
