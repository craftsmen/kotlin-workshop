package nl.craftsmen.workshop.kotlin.app

import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import tornadofx.*

class TasksView : View("Tasks") {

    private val controller: TasksController by inject()

    override val root = vbox {
        buttonbar {
            style {
                padding = box(10.px)
            }
            button("Refresh") {
                action {
                    runAsyncWithProgress {
                        controller.refresh()
                    }
                }
            }
            button("Create") {
                enableWhen(controller.tasks.isNotNull)
                action {
                    replaceWith(TasksFragment::class)
                }
            }
        }
        listview(controller.tasks) {
            vgrow = Priority.ALWAYS
            cellFormat {
                graphic = HBox().apply {
                    val cb = checkbox {
                        tooltip(it.description)
                        isSelected = it.done
                        action {
                            runAsyncWithProgress {
                                controller.update(it.copy(done = isSelected))
                            }
                        }
                    }
                    label(it.name) {
                        textFillProperty().bind(cb.selectedProperty().objectBinding { done ->
                            if (done == true) Color.GRAY else Color.BLACK
                        })
                    }
                }
            }
            onDoubleClick {
                replaceWith(find(TasksFragment::class, params = mapOf(
                        TasksFragment::item to selectedItem
                )))
            }
        }
    }
}
