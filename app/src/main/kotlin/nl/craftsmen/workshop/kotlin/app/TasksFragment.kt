package nl.craftsmen.workshop.kotlin.app

import tornadofx.*

class TasksFragment : Fragment("Create task") {

    val item: Task? by param()

    private val controller: TasksController by inject()
    private val model = TaskViewModel(item ?: Task())

    override val root = form {
        fieldset {
            field("Name") {
                textfield(model.name).validator {
                    when {
                        it == null -> error("This field is required")
                        it.length > Task.maxNameLength -> error("Only ${Task.maxNameLength} characters allowed")
                        else -> null
                    }
                }
            }
            field("Description") {
                textarea(model.description)
            }
        }
        buttonbar {
            button("Save") {
                enableWhen(model.valid)
                action {
                    model.commit {
                        controller.update(model.item)
                    }
                    replaceWith(TasksView::class)
                }
            }
            button("Delete") {
                isDisable = item == null
                action {
                    item?.let {
                        controller.delete(it)
                        replaceWith(TasksView::class)
                    }
                }
            }
        }
    }
}
