package nl.craftsmen.workshop.kotlin.app

import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.ObservableList
import tornadofx.*

class TasksController : Controller() {

    val tasks = SimpleObjectProperty<ObservableList<Task>>()

    private val api: Rest by inject()

    init {
        refresh()
    }

    fun refresh() {
        val response = api.get("")
        if (response.ok()) {
            val newTasks = response.list().toModel<Task>().observable()
            Platform.runLater {
                tasks.value = newTasks
            }
        }
    }

    fun update(task: Task) {
        api.put(task.name, task).consume()
        refresh()
    }

    fun delete(task: Task) {
        api.delete(task.name).consume()
        refresh()
    }
}
