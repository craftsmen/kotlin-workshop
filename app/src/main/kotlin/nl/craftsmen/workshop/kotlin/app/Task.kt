package nl.craftsmen.workshop.kotlin.app

import tornadofx.*
import javax.json.JsonObject

data class Task(var name: String = "",
                var description: String = "",
                var done: Boolean = false) : JsonModel {
    companion object {
        const val maxNameLength = 50
    }

    override fun updateModel(json: JsonObject) {
        name = json.string("name") ?: ""
        description = json.string("description") ?: ""
        done = json.bool("done") == true
    }

    override fun toJSON(json: JsonBuilder) {
        json.add("name", name)
        json.add("description", description)
        json.add("done", done)
    }
}
