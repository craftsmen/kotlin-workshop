package nl.craftsmen.workshop.kotlin.app

import tornadofx.*

class TasksApp : App(TasksView::class) {

    private val rest: Rest by inject()

    init {
        rest.baseURI = "http://localhost:8081/api/tasks"
    }
}
