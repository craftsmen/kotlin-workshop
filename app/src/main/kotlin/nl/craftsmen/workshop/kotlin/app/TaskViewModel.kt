package nl.craftsmen.workshop.kotlin.app

import tornadofx.*
import javax.json.JsonObject

class TaskViewModel() : ItemViewModel<Task>() {

    constructor(item: Task?) : this() {
        this.item = item
    }

    val name = bind(Task::name)
    val description = bind(Task::description)
    val done = bind(Task::done)
}