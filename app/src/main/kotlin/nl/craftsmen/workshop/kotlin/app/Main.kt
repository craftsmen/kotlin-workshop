package nl.craftsmen.workshop.kotlin.app

import javafx.application.Application

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        Application.launch(TasksApp::class.java)
    }
}